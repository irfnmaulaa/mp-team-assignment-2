import { useState } from "react"
import {Appbar, Button, Card, Portal, TextInput, Text, Snackbar} from "react-native-paper"
import {View} from "react-native";
import {useAuth} from "../hooks/Auth";
import styles from "../styles/styles";

export default function Login() {

    const {setIsLoggedIn, isLoggedIn} = useAuth()

    const [state, setState] = useState({
        isError: false,
    })

    const [form, setForm] = useState({
        username: '',
        password: '',
    })

    const handleChange = (name, value) => {
        setForm(prevState => ({
            ...prevState,
            [name]: value,
        }))
    }

    const onDismissSnackBar = () => setState(prevState => ({ ...prevState, visible: false }))

    const signIn = (e) => {
        if (form.username === 'pengguna' && form.password === 'masuk') {
            setIsLoggedIn(true)
        } else {
            setState(prevState => ({
                ...prevState,
                isError: true,
            }))
            setForm(prevState => ({
                ...prevState,
                password: ''
            }))
        }
    }

    return (
        <>

            <Appbar.Header>
                <Appbar.Content title={'Sign In'} />
            </Appbar.Header>

            <View style={styles.container}>

                {
                    state.isError && (
                        <View style={styles.alert.danger}>
                            <Text style={{color: '#FFF'}}>Wrong username or password!</Text>
                        </View>
                    )
                }

                <TextInput
                    mode="outlined"
                    label="Username"
                    placeholder="Enter your username"
                    value={form.username}
                    onChangeText={(text) => {
                        handleChange('username', text)
                    }}
                    style={styles.mb2}
                />

                <TextInput
                    mode="outlined"
                    label="Password"
                    secureTextEntry
                    placeholder="Enter your password"
                    value={form.password}
                    onChangeText={(text) => {
                        handleChange('password', text)
                    }}
                    style={styles.mb4}
                />

                <Button mode="contained" onPress={signIn} style={styles.button}>
                    Sign In
                </Button>

                <Text style={styles.copyright}>
                    &copy; 2023, Group 3 - Mobile Programming
                </Text>
            </View>
        </>
    )
}