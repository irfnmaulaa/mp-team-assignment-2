import {Image, ScrollView, View} from "react-native";
import {Appbar, Button, Text} from "react-native-paper";
import styles from "../styles/styles";
import {useAuth} from "../hooks/Auth";
import Carousel from "../components/Carousel";

export default function Homepage() {

    const {setIsLoggedIn} = useAuth()

    const signOut = () => {
        setIsLoggedIn(false)
    }

    return (
        <>
            <Appbar.Header>
                <Appbar.Content title={'Homepage'} />
            </Appbar.Header>

            <ScrollView style={{
                ...styles.container,
            }}>

                <Carousel/>

                <Button style={{
                    ...styles.button,
                }} onPress={signOut} mode={'contained'}>
                    Sign Out
                </Button>

                <Text style={styles.copyright}>
                    &copy; 2023, Group 3 - Mobile Programming
                </Text>

            </ScrollView>

        </>
    )
}