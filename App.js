
import {AuthProvider} from "./hooks/Auth";
import Screen from "./Screen";

export default function App() {
  return (
      <AuthProvider>
        <Screen/>
      </AuthProvider>
  );
}