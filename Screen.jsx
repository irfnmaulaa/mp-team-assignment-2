import Homepage from "./screens/Homepage";
import Login from "./screens/Login";
import {useAuth} from "./hooks/Auth";
import {MD3LightTheme as DefaultTheme, PaperProvider, Text} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from "@react-navigation/native-stack";

const Stack = createNativeStackNavigator();

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: 'tomato',
        secondary: 'yellow',
    },
};

export default function Screen() {
    const { isLoggedIn } = useAuth()

    return (
        <PaperProvider theme={theme}>
            {
                isLoggedIn ? (
                    <Homepage/>
                ) : (
                    <Login/>
                )
            }
        </PaperProvider>
    )
}