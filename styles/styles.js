import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        padding: 24,
    },
    mb2: {
        marginBottom: 12
    },
    mb4: {
        marginBottom: 24
    },
    button: {
        borderRadius: 6,
        backgroundColor: 'teal',
        padding: 6
    },
    alert: {
        danger: {
            backgroundColor: 'red',
            padding: 16,
            borderRadius: 6,
            marginBottom: 16
        }
    },
    carousel: {
        display: 'flex',
        flexWrap: 'nowrap'
    },
    image: {
        width: '100%',
        height: 200
    },
    copyright: {
        textAlign: 'center',
        marginTop: 44
    }
})

export default styles