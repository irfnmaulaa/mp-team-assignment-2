import * as React from 'react';
import {
    Text,
    View,
    SafeAreaView, Image
} from 'react-native';

import Carousel from 'react-native-snap-carousel';
import styles from "../styles/styles";

export default class extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            activeIndex:0,
            carouselItems: [
                {
                    title:"Bali",
                    text: "Indonesia",
                    image: require(`../images/image1.png`),
                },
                {
                    title:"Phuket",
                    text: "Thailand",
                    image: require(`../images/image2.png`),
                },
                {
                    title:"Raja Ampat",
                    text: "Indonesia",
                    image: require(`../images/image3.png`),
                },
            ]
        }
    }

    _renderItem({item,index}){
        return (
            <View style={{
                borderRadius: 5,
                marginLeft: 25,
                marginRight: 25,
                backgroundColor: '#f5f5f5',
                overflow: 'hidden'
            }}>

                <Image source={item.image} style={styles.image}/>

                <View style={{
                    padding: 24
                }}>
                    <Text style={{fontSize: 21, textAlign: 'center', marginBottom: 6}}>{item.title}</Text>
                    <Text style={{textAlign: 'center'}}>{item.text}</Text>
                </View>
            </View>

        )
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, marginBottom: 16, borderStyle: 'solid', borderWidth: 1, borderColor: '#f5f5f5' }}>
                <View style={{ flex: 1, flexDirection:'row', justifyContent: 'center', paddingTop: 22, paddingBottom: 22 }}>
                    <Carousel
                        layout={"default"}
                        ref={ref => this.carousel = ref}
                        data={this.state.carouselItems}
                        sliderWidth={300}
                        itemWidth={300}
                        renderItem={this._renderItem}
                        onSnapToItem = { index => this.setState({activeIndex:index}) } />
                </View>
            </SafeAreaView>
        );
    }
}

